// Exercices sur la création de fonction
// Toutes les réponses vont s'afficher dans la partie gauche du footer

// Selection de boite dans footer
const link_footer_1 = document.querySelector('#link_footer_1');
const link_footer_2 = document.querySelector('#link_footer_2');
const link_footer_3 = document.querySelector('#link_footer_3');
const link_footer_4 = document.querySelector('#link_footer_4');
const link_footer_5 = document.querySelector('#link_footer_5');

/////////////////////
// Exo 1
////////////////////
// Modifier la fonction "showDateInFrench" pour qu'elle renvoie la date du jour sous la forme d'une chaine du type "Le 2 décembre 2022"
// La méthode getMonth(); renvoie un chiffre entier (0 pour janvier, 11 pour décembre)

function showDateInFrench() {
    tableMois = ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'];
    const date = new Date();
    const jour = date.getDate();
    const mois =  tableMois[date.getMonth()];
    const annee = date.getFullYear();
    return 'Le ' + jour + ' ' + mois + ' ' + annee
}
link_footer_1.innerText = showDateInFrench();
/////////////////////
// Exo 2
////////////////////
// Modifiez la fonction "isTriangle" pour qu'elle prenne des arguments et renvoie true si c’est un triangle, sinon false.
// Utilisez le théorème d’inégalité. La somme des longueurs de deux côtés d’un triangle est toujours supérieure au troisième côté. Si cela est vrai pour les trois combinaisons, vous avez un triangle.
// isTriangle(34,37,58) => true
// isTriangle(134,77,48) => false
function isTriangle(num1,num2,num3) {
    if (num1 + num2 > num3 && num1 + num3 > num2 && num2 + num3 > num1){
        return true;
    }
    return false;
}
link_footer_2.innerText = isTriangle(134,77,48);
/////////////////////
// Exo 3
////////////////////
// Modifiez la fonction "generateStringUser" qui prend en paramètres l'objet "user" et qui renvoie une chaine du type "Michel a 45 ans. Il habite à Pont-Audemer."
const user = {prenom: "Michel", age: 45, ville: "Pont-Audemer"};
function generateStringUser(user) {
    return user.prenom + ' a ' + user.age + ' ans. Il habite à ' + user.ville + '.';
}
link_footer_3.innerText = generateStringUser(user);
//////////////
// Exo 4
//////////////
// Vous devez modifier la fonction "getAJoke" pour aller chercher une blague au hasard grâce à cette API et à fetch().
// https://joke.deno.dev

// Vous devez retourner de cette fonction une blague, afin de faire afficher la question dans un li et la punchline dans un autre.
function getAJoke() {
    fetch('https://joke.deno.dev')
        .then(function (response){
            return response.json();
        })
        .then(function (data){
            link_footer_4.innerText = data["setup"];
            link_footer_5.innerText = data["punchline"];
        })
        .catch(function (error){
            console.log(error)
        })
}
getAJoke();
