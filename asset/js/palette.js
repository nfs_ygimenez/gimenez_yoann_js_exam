// Ici mettre le code js pour la palette
const body = document.querySelector('body')
const openPalette = document.querySelector('#link_palette')
const palette = document.querySelector('#palette');
const inputPalette = document.querySelector('#inputPalette')

openPalette.addEventListener('click', function (event){
    event.preventDefault();
    palette.classList.toggle('openPalette')
})

inputPalette.addEventListener('change', function (event){
    palette.classList.remove('openPalette')
    body.style.backgroundColor = inputPalette.value
})