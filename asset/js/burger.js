// Ici mettre le code js pour le menu burger

const openBurger = document.querySelector('#link_burger');
const menuBurger = document.querySelector('#nav_burger')
const closeBurger = document.querySelector('.close')

openBurger.addEventListener('click', function (event){
    event.preventDefault();
    menuBurger.classList.add('openBurger')
})
closeBurger.addEventListener('click', function (event){
    event.preventDefault();
    menuBurger.classList.remove('openBurger')
})