// Ici mettre le code js pour le dropdown
    const ingredients = [
    {title: 'Carotte', img: 'slide5.jpg', icone: 'carrot', content: 'La Carotte (Daucus carota subsp. sativus) est une plante bisannuelle de la famille des Apiacées (aussi appelées Ombellifères), largement cultivée pour sa racine pivotante charnue, comestible, de couleur généralement orangée, consommée comme légume. La carotte représente, après la pomme de terre, le principal légume-racine cultivé dans le monde2. C\'est une racine riche en carotène.'},
    {title: 'Poisson', img: 'slide4.jpg', icone: 'fish',content: 'Les poissons sont des animaux vertébrés aquatiques à branchies, pourvus de nageoires dont le corps est généralement couvert d\'écailles. On les trouve abondamment aussi bien dans les eaux douces, saumâtres et de mers : on trouve des espèces depuis les sources de montagnes (omble de fontaine, goujon) jusqu\'au plus profond des mers et océans (grandgousier, poisson-ogre). Leur répartition est toutefois très inégale : 50 % des poissons vivraient dans 17 % de la surface des océans1 (qui sont souvent aussi les plus surexploités).'},
    {title: 'Piment', img: 'slide1.jpg', icone: 'pepper',content: 'Le terme piment (vert, jaune, orange, rouge, brun, pêche ou violet) est un nom vernaculaire désignant le fruit de cinq espèces de plantes du genre Capsicum de la famille des Solanacées et de plusieurs autres taxons. Le mot désigne plus communément le fruit de ces plantes, utilisés comme condiment ou légume (en français canadien, le mot piment désigne parfois les poivrons, les autres variétés de Capsicum, au goût plus piquant, étant appelés piments forts). La notion de piment est généralement associée à la saveur de piquant.'},
    {title: 'Citron', img: 'slide3.jpg', icone: 'lemon', content: 'Le citron (ou citron jaune) est un agrume, fruit du citronnier (Citrus limon). Il existe sous deux formes : le citron doux, fruit décoratif de cultivars à jus peu ou pas acide néanmoins classé Citrus limon (L.) Burm. f. (classification de Tanaka) ; et le citron acide, le plus commun de nos jours, dont le jus a un pH d\'environ 2,5.'},
    {title: 'Crevette', img: 'slide2.jpg', icone: 'crevette', content: 'Le nom vernaculaire crevette (aussi connu comme chevrette dans certaines régions de la francophonie) est traditionnellement donné à un ensemble de crustacés aquatiques nageurs, essentiellement marins mais aussi dulcicoles, autrefois regroupés dans le sous-ordre des « décapodes nageurs », ou Natantia.'},
];

const boite = document.querySelector('#dropdown')
const button = document.querySelector('#dropdown a')
const response = document.querySelector('#reponse')
const dropdown = document.querySelector('#dropdown ul')
const icone = document.createElement('img')
icone.src = 'asset/img/icone_carrot.svg'
boite.appendChild(icone)

button.addEventListener('click',function (event){
    event.preventDefault()
    dropdown.classList.toggle('show')
    dropdown.innerHTML = '';
    for (let i = 0; i < ingredients.length; i++){
        const li = document.createElement('li')
        li.innerText = ingredients[i]["title"]
        dropdown.appendChild(li)
        li.addEventListener('click',function (event){
            response.innerHTML = ''
            button.innerText = li.innerText;
            dropdown.classList.remove('show')
            texteIngredient = document.createElement('div')
            response.appendChild(texteIngredient)
            titleIngredient = document.createElement('h2')
            titleIngredient.innerText = ingredients[i]["title"]
            texteIngredient.appendChild(titleIngredient)
            texteIngredient.classList.add('texte_ingredient')

            descIngredient = document.createElement('p')
            descIngredient.innerText = ingredients[i]["content"]
            texteIngredient.appendChild(descIngredient)
            divImage = document.createElement('div')
            response.appendChild(divImage)
            imgIngredient = document.createElement('img')
            imgIngredient.src = 'asset/img/' + ingredients[i]["img"]
            divImage.appendChild(imgIngredient)
            icone.src = 'asset/img/icone_' + ingredients[i]["icone"] + '.svg'

        })
    }
})

    button.innerText = ingredients[0]["title"];
    dropdown.classList.remove('show')
    texteIngredient = document.createElement('div')
    response.appendChild(texteIngredient)
    titleIngredient = document.createElement('h2')
    titleIngredient.innerText = ingredients[0]["title"]
    texteIngredient.appendChild(titleIngredient)
    descIngredient = document.createElement('p')
    descIngredient.innerText = ingredients[0]["content"]
    texteIngredient.appendChild(descIngredient)
    texteIngredient.classList.add('texte_ingredient')
    divImage = document.createElement('div')
    response.appendChild(divImage)
    imgIngredient = document.createElement('img')
    imgIngredient.src = 'asset/img/' + ingredients[0]["img"]
    divImage.appendChild(imgIngredient)

